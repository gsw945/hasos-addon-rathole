#!/bin/bash
set -eux

# ensure config
if [ ! -d "/root/config" ]; then mkdir -p "/root/config" ; fi
if [[ -z "$(ls -A -- "/root/config")" ]] ; then cp -r "/root/__default_config__/config" "/root/" ; fi

# run
/root/rathole --$RATHOLE_TYPE "/root/config/${RATHOLE_TYPE}.toml"

# Debug Dockerfile
# while [ 1 ]
# do
#     echo "log ..."
#     echo "type: $RATHOLE_TYPE"
#     echo "type2: ${RATHOLE_TYPE}"
#     sleep 5s
# done
