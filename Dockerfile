ARG BUILD_FROM=ghcr.io/hassio-addons/base/aarch64:12.2.3
# hadolint ignore=DL3006
FROM ${BUILD_FROM}

# Set shell
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Add env
ENV TERM="xterm-256color"

# Setup base
ARG BUILD_ARCH=aarch64
# hadolint ignore=DL3003,DL3042

COPY config /root/__default_config__/config
COPY start.sh /root/
WORKDIR /root

RUN wget -O /root/rathole-arm-unknown-linux-musleabi.zip -c https://ghproxy.com/https://github.com/rapiz1/rathole/releases/download/v0.4.3/rathole-arm-unknown-linux-musleabi.zip
RUN unzip /root/rathole-arm-unknown-linux-musleabi.zip
RUN rm /root/rathole-arm-unknown-linux-musleabi.zip

# "server" or "client"
ENV RATHOLE_TYPE="client"

ENTRYPOINT ["/bin/bash", "/root/start.sh"]

# ref: https://docs.docker.com/engine/reference/commandline/build/#options
# docker build --progress plain --rm --tag gsw945/addon-rathole:0.0.1 .
# ref: https://docs.docker.com/engine/reference/commandline/run/#options
# docker run --rm -it -v "/mnt/data/supervisor/media/rathole-config":"/root/config" --name rathole -e RATHOLE_TYPE=client -d gsw945/addon-rathole:0.0.1
# docker logs --follow --tail 10 rathole
# docker exec -it rathole /bin/bash
